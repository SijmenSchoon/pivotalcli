import argparse
import sys
from typing import Dict

import base32_crockford
import tabulate

import api.projects
from config import Config
from util import require_login


@require_login
def list_projects(arguments: argparse.Namespace) -> None:
    projects = api.projects.get(Config['user']['api_token'])
    projects.sort(key=lambda project: project['name'])

    aliases: Dict[int, str] = {}
    for alias, alias_id in Config['project_aliases'].items():
        aliases[int(alias_id)] = alias

    table = []
    for project in sorted(projects, key=lambda a: a['name']):
        code = base32_crockford.encode(project['id'])
        alias = aliases.get(project['id'])
        table.append((code, project['name'], alias))

    print(tabulate.tabulate(table, headers=('Code', 'Name', 'Alias')))


def alias(arguments: argparse.Namespace) -> None:
    project_id = base32_crockford.decode(arguments.code)
    Config['project_aliases'][arguments.alias] = str(project_id)
    Config.write()


def rmalias(arguments: argparse.Namespace) -> None:
    del Config['project_aliases'][arguments.alias]
    Config.write()


def info(arguments: argparse.Namespace) -> None:
    try:
        token = Config['user']['api_token']
        project_id = int(Config['project_aliases'][arguments.alias])
    except KeyError:
        print(f'unknown alias {arguments.alias}')
        sys.exit(1)

    projects = api.projects.get_project(token, project_id).items()
    print(tabulate.tabulate(projects))
