import argparse
import getpass

import api.me
from config import Config


def login(arguments: argparse.Namespace) -> None:
    username = input('E-mail: ')
    password = getpass.getpass()

    me = api.me.get(username, password)

    Config['user']['api_token'] = me['api_token']
    Config['user']['initials'] = me['initials']
    Config.write()
