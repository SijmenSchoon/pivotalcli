import api.projects
from typing import Dict, Any
from util import Color

COLOR_TITLE = Color(Color.YELLOW)
COLOR_HEADER = Color(Color.CYAN)
COLOR_WHITE = Color(Color.BRIGHT_WHITE)


def _format_state(state: str) -> str:
    STATES = {
        'accepted': '\033[92maccepted\033[0m',
        'delivered': '\033[38;5;208mdelivered\033[0m',
        'finished': '\033[94mfinished\033[0m',
        'started': '\033[38;5;226mstarted\033[0m',
        'planned': '\033[90mplanned\033[0m',
        'unstarted': '\033[90munstarted\033[0m',
    }

    return STATES[state]


def _get_persons(token: str, project_id: int) -> Dict[int, Dict[str, Any]]:
    memberships = api.projects.get_memberships(token, project_id)

    persons: Dict[int, Dict[str, Any]] = {}
    for membership in memberships:
        person = membership['person']
        persons[person['id']] = person

    return persons
