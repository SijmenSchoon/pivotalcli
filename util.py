import commands.login
import shutil
import textwrap
from typing import Any, Callable, Optional

from config import Config


class Color:
    BLACK = 30
    RED = 31
    GREEN = 32
    YELLOW = 33
    BLUE = 34
    MAGENTA = 35
    CYAN = 36
    WHITE = 37
    BRIGHT_BLACK = 90
    BRIGHT_RED = 91
    BRIGHT_GREEN = 92
    BRIGHT_YELLOW = 93
    BRIGHT_BLUE = 94
    BRIGHT_MAGENTA = 95
    BRIGHT_CYAN = 96
    BRIGHT_WHITE = 97

    RESET = 0

    def __init__(self, foreground: Optional[int],
                 background: Optional[int] = None) -> None:
        self.foreground = foreground
        self.background = background

    def print(self, *args: Any, end: str = '\n') -> None:
        print(self.format(*args), end=end)

    def format(self, *args: Any) -> str:
        colors = ['0']

        if self.foreground is not None:
            colors.append(str(self.foreground))

        if self.background is not None:
            colors.append(str(self.background + 10))

        color_str = ';'.join(colors)
        text = ' '.join(str(a) for a in args)

        return f'\033[{color_str}m{text}\033[0m'


def print_wrap(text: str, indent: str = '', end: str = '\n') -> None:
    w, _ = shutil.get_terminal_size((80, 20))
    if w > 72:
        w = 72

    lines = text.split('\n')
    for i, line in enumerate(lines):
        _end = '\n' if i < len(lines) - 1 else end

        if not line:
            print(indent, end=_end)
            continue

        wrapped = textwrap.fill(
            line, w, initial_indent=indent, subsequent_indent=indent)
        print(wrapped, end=_end)


def require_login(function: Callable) -> Callable:
    def wrapper(*args: Any, **kwargs: Any) -> Any:
        while 'api_token' not in Config['user']:
            commands.login.login(None)

        return function(*args, **kwargs)

    return wrapper
