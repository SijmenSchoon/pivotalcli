#!/usr/bin/env python3
import argparse
import commands.login as cmd_login
import commands.projects as cmd_projects
import commands.stories as cmd_stories
import api.stories

from config import Config
from base32_crockford import decode as b32_decode


def start_story(args) -> None:
    story_set_state(args.story, 'finished')


def finish_story(args) -> None:
    story_set_state(args.story, 'finished')


def deliver_story(args) -> None:
    story_set_state(args.story, 'delivered')


def accept_story(args) -> None:
    story_set_state(args.story, 'accepted')


def story_set_state(args, state: str) -> None:
    token = Config['user']['api_token']
    story_id = b32_decode(args.story)
    api.stories.put_story(token, story_id, current_state=state)


def parse_arguments() -> None:
    parser = argparse.ArgumentParser()
    parser.set_defaults(func=lambda _: parser.print_help())
    commands = parser.add_subparsers(title='commands')

    login_parser = commands.add_parser('login')
    login_parser.set_defaults(func=cmd_login.login)

    projects_parser = commands.add_parser('projects')
    projects_parser.set_defaults(func=cmd_projects.list_projects)

    projects_commands = projects_parser.add_subparsers(title='commands')
    projects_list_parser = projects_commands.add_parser('list')
    projects_list_parser.set_defaults(func=cmd_projects.list_projects)

    projects_alias_parser = projects_commands.add_parser('alias')
    projects_alias_parser.add_argument('code', type=str)
    projects_alias_parser.add_argument('alias', type=str)
    projects_alias_parser.set_defaults(func=cmd_projects.alias)

    projects_rmalias_parser = projects_commands.add_parser('rmalias')
    projects_rmalias_parser.add_argument('alias', type=str)
    projects_rmalias_parser.set_defaults(func=cmd_projects.rmalias)

    projects_info_parser = projects_commands.add_parser('info')
    projects_info_parser.add_argument('alias', type=str)
    projects_info_parser.set_defaults(func=cmd_projects.info)

    stories_parser = commands.add_parser('stories', description='story stuff')
    stories_parser.set_defaults(func=cmd_stories.stories)
    stories_parser.add_argument('project', type=str)
    stories_parser.add_argument('story', type=str, nargs='?', default=None)
    stories_parser.add_argument('--scope', type=str, default='current')
    stories_parser.add_argument('--hide-accepted', nargs='?', type=bool,
                                const=True, default=False)

    story_start_parser = commands.add_parser('start')
    story_start_parser.set_defaults(
        func=lambda args: story_set_state(args, 'started'))
    story_start_parser.add_argument('story', type=str)

    story_finish_parser = commands.add_parser('finish')
    story_finish_parser.set_defaults(
        func=lambda args: story_set_state(args, 'finished'))
    story_finish_parser.add_argument('story', type=str)

    story_deliver_parser = commands.add_parser('deliver')
    story_deliver_parser.set_defaults(
        func=lambda args: story_set_state(args, 'delivered'))
    story_deliver_parser.add_argument('story', type=str)

    story_accept_parser = commands.add_parser('accept')
    story_accept_parser.set_defaults(
        func=lambda args: story_set_state(args, 'accepted'))
    story_accept_parser.add_argument('story', type=str)

    args = parser.parse_args()
    args.func(args)


if __name__ == '__main__':
    Config.read()
    parse_arguments()
