from datetime import datetime
from typing import Any, Dict, List, Optional

import requests


def get(token: str) -> List[Dict[str, Any]]:
    r = requests.get(
        f'https://www.pivotaltracker.com/services/v5/projects',
        headers={'X-TrackerToken': token})
    return r.json()


def get_project(token: str, project_id: int) -> Dict[str, Any]:
    r = requests.get(
        f'https://www.pivotaltracker.com/services/v5/projects/{project_id}',
        headers={'X-TrackerToken': token})
    return r.json()


def get_stories(token: str, project_id: int) -> List[Dict[str, Any]]:
    r = requests.get(
        f'https://www.pivotaltracker.com/services/v5/projects/{project_id}'
        '/stories', headers={'X-TrackerToken': token})
    return r.json()


def get_memberships(token: str, project_id: int) -> List[Dict[str, Any]]:
    r = requests.get(
        f'https://www.pivotaltracker.com/services/v5/projects/{project_id}'
        '/memberships', headers={'X-TrackerToken': token})
    return r.json()


def get_iterations(token: str, project_id: int, scope: str) \
        -> List[Dict[str, Any]]:
    r = requests.get(
        f'https://www.pivotaltracker.com/services/v5/projects/{project_id}'
        f'/iterations?scope={scope}', headers={'X-TrackerToken': token})
    return r.json()


def get_story_transitions(token: str, project_id: int,
                          after: Optional[datetime] = None,
                          before: Optional[datetime] = None) \
        -> List[Dict[str, Any]]:
    parameters = {
        'occurred_after': after.isoformat() if after else None,
        'occurred_before': before.isoformat() if before else None,
    }

    r = requests.get(
        f'https://www.pivotaltracker.com/services/v5/projects/{project_id}'
        f'/story_transitions',
        headers={'X-TrackerToken': token}, 
        params=parameters)
    return r.json()


def get_history_days(token: str, project_id: int, start: datetime) \
        -> Dict[str, Any]:
    parameters = {'start_date': start.isoformat()}

    r = requests.get(
        f'https://www.pivotaltracker.com/services/v5/projects/{project_id}'
        f'/history/days', headers={'X-TrackerToken': token}, params=parameters)
    return r.json()
