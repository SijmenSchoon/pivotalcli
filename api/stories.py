import requests
from typing import Dict, Any, List


def get(token: str, story_id: int = None) -> Dict[str, Any]:
    r = requests.get(
        f'https://www.pivotaltracker.com/services/v5/stories/{story_id}',
        headers={'X-TrackerToken': token})
    return r.json()


def put_story(token: str, story_id: int, **kwargs) -> Dict[str, Any]:
    r = requests.put(
        f'https://www.pivotaltracker.com/services/v5/stories/{story_id}',
        headers={'X-TrackerToken': token}, json=kwargs)
    print(r.text)
    return r.json()


def get_tasks(token: str, project_id: int, story_id: int) \
        -> List[Dict[str, Any]]:
    r = requests.get(
        f'https://www.pivotaltracker.com/services/v5/projects/{project_id}'
        f'/stories/{story_id}/tasks', headers={'X-TrackerToken': token})
    return r.json()


def get_comments(token: str, project_id: int, story_id: int) \
        -> List[Dict[str, Any]]:
    r = requests.get(
        f'https://www.pivotaltracker.com/services/v5/projects/{project_id}'
        f'/stories/{story_id}/comments', headers={'X-TrackerToken': token})
    return r.json()


def get_blockers(token: str, project_id: int, story_id: int) \
        -> List[Dict[str, Any]]:
    r = requests.get(
        f'https://www.pivotaltracker.com/services/v5/projects/{project_id}'
        f'/stories/{story_id}/blockers', headers={'X-TrackerToken': token})
    return r.json()
