import requests
from requests.auth import HTTPBasicAuth
from typing import Dict, Any


def get(username: str, password: str) -> Dict[str, Any]:
    r = requests.get('https://www.pivotaltracker.com/services/v5/me', 
                     auth=HTTPBasicAuth(username, password))
    return r.json()
