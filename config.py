import configparser
import os
from typing import Dict

import appdirs


class _Config(type):
    def __getitem__(cls, key: str) -> Dict[str, str]:
        if key not in cls.config:
            cls.config[key] = {}

        return cls.config[key]


class Config(object, metaclass=_Config):
    config = configparser.ConfigParser()

    @staticmethod
    def __get_filename() -> str:
        data_dir = appdirs.user_data_dir('pivotalcli', 'sijmen')
        os.makedirs(data_dir, exist_ok=True)
        return os.path.join(data_dir, 'config.ini')

    @classmethod
    def read(cls) -> None:
        cls.config.read(cls.__get_filename())

    @classmethod
    def write(cls) -> None:
        with open(cls.__get_filename(), mode='w') as config_file:
            cls.config.write(config_file)
